<?php

namespace space\modules\file_manager\models;

use Exception;
use space\kernel\core\Model;
use space\kernel\core\WPS;

/**
 * Class FileManager
 * @property int    $id
 * @property int    $user_id
 * @property int    $site_id
 * @property string $path
 * @property int    $access
 * @property int    $mime_type
 * @property int    $date_time
 * @property string $hash
 */
class FileManager extends Model
{

	/**
	 * Allowed extensions
	 */
	const ALLOWED_MIME_TYPES = [
		"application/msword", //doc
		"application/vnd.ms-excel", //xls
		"application/vnd.ms-office",
		"application/vnd.ms-powerpoint", //"ppt","pps",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document", //"docx",
		"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",//"xlsx",
		"application/vnd.openxmlformats-officedocument.presentationml.presentation",//"pptx",
		"application/vnd.openxmlformats-officedocument.presentationml.slideshow",//"ppsx",
		"application/pdf",//"pdf",
		"application/x-rar-compressed",//"rar",
		"application/zip",//"zip",
		"image/pjpeg",//"pjpeg",
		"image/jpg",//"jpg",
		"image/jpeg",//"jpeg",
		"image/gif",//"gif",
		"image/png",//"png",
		"text/plain",//"txt",log
	];

	/** @inheritdoc */
	public static function tableName(): string
	{
		return 'file_manager';
	}

	/** @inheritdoc */
	public static function attributes(): array
	{
		return [
			'id',
			'user_id',
			'site_id',
			'path',
			'access',
			'hash',
			'mime_type',
			'date_time',
		];
	}

	/**
	 * Return a full path to file
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	private static function getFullPath(string $path): string
	{
		return DOCUMENT_ROOT . "/" . $path;

	}

	/**
	 * Register file in DB
	 *
	 * @param string $file_path
	 * @param int    $access_level
	 *
	 * @return FileManager
	 * @throws Exception
	 */
	public static function registerFile(string $file_path, int $access_level = 0): self
	{
		$full_path = self::getFullPath($file_path);
		if (!file_exists($full_path)) {
			throw new Exception('File ' . $full_path . " is not exists ");
		}

		if (!self::isAllowedMimeType($full_path)) {
			throw new Exception('File type is not allowed', 403);
		}

		$model = new self();
		$model->site_id = WPS::getSiteId();
		$model->user_id = WPS::$app->user->id;
		$model->path = $file_path;
		$model->mime_type = self::getMimeType($full_path);
		$model->access = $access_level;
		$model->date_time = date('Y-m-d H:i:s');
		$model->hash = md5(WPS::getSiteId() . WPS::$app->user->id . $file_path . $access_level . time());
		$model->insert();

		return $model;
	}

	/**
	 * Check file type
	 *
	 * @param string $path
	 *
	 * @return bool
	 */
	private static function isAllowedMimeType(string $path): bool
	{
		$mime_type = self::getMimeType($path);

		return in_array($mime_type, self::ALLOWED_MIME_TYPES);
	}

	/**
	 * Find file by hash
	 *
	 * @param string $hash
	 *
	 * @return FileManager|null
	 */
	public static function getByHash(string $hash):?self
	{
		$row = self::find(["hash" => $hash]);
		if (!count($row)) {
			return null;
		}
		$model = new self();
		$model->setAttributes($row[0]);

		return $model;
	}

	/**
	 * Find files by condition or id
	 *
	 * @param $idOrCondition
	 *
	 * @return array
	 */
	private static function find($idOrCondition): array
	{
		$tableName = WPS::$app->db->normalizeTableName(self::tableName());
		$condition = WPS::$app->db->prepareCondition($idOrCondition);
		$sql = "SELECT * FROM `$tableName` WHERE $condition";
		$res = WPS::$app->db->get_array($sql);

		return $res;
	}

	/**
	 * Get file mime type
	 *
	 * @param string $path
	 *
	 * @return null|string
	 */
	protected static function getMimeType(string $filename):?string
	{
		$finfo = finfo_open(FILEINFO_MIME_TYPE); // возвращает mime-тип
		$mim_type = finfo_file($finfo, $filename);
		finfo_close($finfo);

		if ($mim_type == false) {
			return null;
		}

		return $mim_type;
	}

	/**
	 * Send file to browser
	 */
	public function sendFileToBrowser(): void
	{
		header('content-type: ' . $this->mime_type);
		$file_type = explode("/", $this->mime_type);
		if ($file_type != 'image') {
			header('Content-Disposition: inline; filename="' . array_pop(explode('/', $this->path)) . '"');
		}

		$file = self::getFullPath($this->path);
		readfile($file);
	}
}