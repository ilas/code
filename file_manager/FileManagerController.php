<?php
namespace space\modules\file_manager;
use space\kernel\KernelController;
use space\modules\file_manager\models\FileManager;
use space\kernel\core\WPS;



class FileManagerController extends KernelController
{

	/**
	 * Gives file by  hash
	 *
	 * @throws \Exception
	 */
	public function actionIndex()
	{

		$hash = WPS::$app->request->get('id');
		if (!$hash) {
			throw new \Exception("Not found", 404);
		}

		$file = FileManager::getByHash($hash);

		if (is_null($file)) {
			throw new \Exception("Not found", 404);
		}

		if ($file->access > WPS::$app->user->access) {
			throw new \Exception("Access denied", 403);
		}
		$file->sendFileToBrowser();

	}


}