import React, { Component, PropTypes } from 'react';
import {
  WebView,
  TouchableHighlight,
  Text,
  View,
  Image,
  StyleSheet,
  PixelRatio
} from 'react-native';
import Share, {ShareSheet, Button} from 'react-native-share';
import { Icon } from 'react-native-material-design';
import Drawer from './DrawerHTML'


export default class CanvasDrawer extends Component {

  constructor( props ) {
       super( props );
       this.webview = null;
         this.state={image: null};
   }



  onMessage( event ) {
       this.setState({image: event.nativeEvent.data})
   }

   onError(e){
     console.log( "error", e );
   }



   onLoad(){
     console.log('load');
   }

   requesImage(){

      //this.refs[WEBVIEW_REF].sendMessage('get_image')
      this.webview.postMessage('get_image')
   }



  render(){

    if(this.state.image){

      console.log(this.state.image);

      return(
        <View style={{flex:1}}>
        <Image  style={styles.avatar} source={{uri:this.state.image}}/>
        </View>
      )
    }

     //console.log('draver props  => ',this.props.uri);

     //Share.shareSingle({url: this.props.uri, message: '', "social": "instagram"})


    let content = require('./DrawerHTML');

    let js =  require('./DrawerJs').default.replace('%img_uri%',this.props.uri);

    //console.log(content);

    return(
      <View style={{flex:1}}>
      <WebView
        ref={wv => { this.webview = wv; }}
        source={{html: Drawer}}
        renderError={(e)=>{console.log(e)}}
        javaScriptEnabled={true} startInLoadingState={true}  domStorageEnabled={true}
        onMessage={this.onMessage.bind(this)}
        onError={this.onError}
        injectedJavaScript={js}
        onLoad={this.onLoad}

       />

       <TouchableHighlight underlayColor='#fff' onPress={ this.requesImage.bind(this) }>
         <Text>
           <Icon name="save"   />
         </Text>
       </TouchableHighlight>
       </View>
     );

  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar: {
    borderRadius: 0,
    width: 300,
    height: 300
  }
});
