export default `


var canvas = document.querySelector('#paint');
  var ctx = canvas.getContext('2d');
function sendImg(){
   window.postMessage(canvas.toDataURL(), "*");
}


  document.addEventListener("message", function(event) {

    var msg = event.data;
      alert("Received post message: "+  msg);

      if(msg=='get_image'){
        sendImg();
      }

  }, false);


  var image = new Image();
image.onload = function() {
    ctx.drawImage(image, 0, 0);
};
image.src = "%img_uri%";

  var sketch = document.querySelector('#sketch');

  var sketch_style = getComputedStyle(sketch);
  canvas.width = parseInt(sketch_style.getPropertyValue('width'));
  canvas.height = parseInt(sketch_style.getPropertyValue('height'));

  var mouse = {x: 0, y: 0};



  var start_events = ["touchstart"];
  var move_events = [ "touchmove"];
  var end_events = [ "touchend"];


  move_events.forEach(function(event) {
    canvas.addEventListener(event, function(e) {
      var touch = e.touches[0];
      var x = touch.pageX;
      var y = touch.pageY;
      mouse.x = x - this.offsetLeft;
      mouse.y = y - this.offsetTop;
    }, false);
  });




  ctx.lineWidth = 5;
  ctx.lineJoin = 'round';
  ctx.lineCap = 'round';
  ctx.strokeStyle = 'back';


  start_events.forEach(function(event) {
    canvas.addEventListener(event, function(e) {

      move_events.forEach(function(me){ canvas.addEventListener(me, onPaint, false); });
    }, false);
  });


  end_events.forEach(function(event) {
    canvas.addEventListener(event, function(e) {

      move_events.forEach(function(me){ canvas.removeEventListener(me, onPaint, false); });
    }, false);
  });


  var onPaint = function() {
    ctx.beginPath();
    ctx.globalCompositeOperation="destination-out";
    ctx.arc(mouse.x,mouse.y,50,0,Math.PI*2,false);
    ctx.fill();
    ctx.closePath();
  };
  `;
